//
// Created by iddqd13 on 5/17/18.
//

#ifndef PROJECT_KEYSGENERATOR_H
#define PROJECT_KEYSGENERATOR_H


#include <QtCore/QString>
#include <utils.h>

namespace openssl {
    /**
     * Helper class, generate and store keys, can generate address
     * can write it to output or to file ( if outDir specified)
     */
    class KeysGenerator {
    public:
        /**
         * constructor with outDir, which indicates keys store to directory
         * @param outDir destination directory for resulting files.
         */
        explicit KeysGenerator(const QString& outDir = QString());

        /** main method for generate keys(public|private) **/
        void generateKeys();

    protected:
        /**
         * This is overloaded method;
         * Print value with optional name and description
         * @param data vectorChar(std::vector<char>) data for print;
         * @param optionalName maybe empty - string before data printing;
         * @param optionalPrefix maybe empty - string before data printing;
         */
        void printValue(vectorChar& data
                , const QString& optionalName = QString()
                , const QString& optionalPrefix = QString() );

        /**
         * This is overloaded method;
         * Print value with optional name and description
         * @param data vectorUchar(std::vector<unsigned char>) data for print;
         * @param optionalName maybe empty - string before data printing;
         * @param optionalPrefix maybe empty - string before data printing;
         */
        void printValue(vectorUchar& data
                , const QString& optionalName = QString()
                , const QString& optionalPrefix = QString() );

        /**
         * Calculate SHA-256 value from input data and store to output.
         * internal call for dst.resize.
         * @param src vectorUchar(std::vector<unsigned char>) data for calculating
         * @param dst vectorUchar(std::vector<unsigned char>) data for store result
         */
        void calculateSHA256(vectorUchar& src , vectorUchar& dst );
        /**
         * Calculate RMD-160 value from input data and store to output.
         * internal call for dst.resize.
         * @param src vectorUchar(std::vector<unsigned char>) data for calculating
         * @param dst vectorUchar(std::vector<unsigned char>) data for store result
         */
        void calculateRmd160(vectorUchar& src , vectorUchar& dst );

        /**
         * Read data from BIO_mem to output vector
         * @param input (type = BioUniquePtr ), pass by reference
         * @param output vectorUchar(std::vector<unsigned char>) data for store result
         */
        void readBioToVector(BIOUniquePtr& input , vectorUchar& output );
        /**
         * Read data from BIO_mem to output vector
         * @param input (type = BioUniquePtr ), pass by reference
         * @param output vectorChar(std::vector<char>) data for store result
         */
        void readBioToVector(BIOUniquePtr& input, vectorChar& output );

        /** internal method for initializing members **/
        void init();

        /**  convert input vector to String, internal method. **/
        QString convertToString(vectorUchar& data
                , const QString& optionalName = QString()
                , const QString& optionalPrefix = QString() );
        QString convertToString(vectorChar& data
                , const QString& optionalName = QString()
                , const QString& optionalPrefix = QString() );

        /**
         * save generated values to files;
         */
        void saveToFiles();

        /**
         * print generated values to console;
         */
        void printToConsole();

        /**
         * Save given content to specified fileName
         * @param fileName absolute path for file ( with extension)
         * If filename exists, this method overwrites file
         * @param content file content
         */
        bool saveFile(const QString& fileName , const QString& content);
    private:
        /**  output Directory **/
        QString m_outDir;

        /** Smart Pointer to EC_GROUP **/
        EC_GROUPUniquePtr m_curveGroup;
        /**  **/
        point_conversion_form_t m_form;
        /** smart Pointer to EC_KEY **/
        EC_KEYUniquePtr m_eckey;

        /** private key, each call generateKeys method generates private and public keys **/
        Key m_private;
        /** public key, each call generateKeys method generates private and public keys **/
        Key m_public;
        /** address, generated from private and public keys , don't have prefix with 0x **/
        vectorUchar m_address;
    };
}

#endif //PROJECT_KEYSGENERATOR_H
