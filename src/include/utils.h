//
// Created by iddqd13 on 5/21/18.
//

#ifndef PROJECT_UTILS_H
#define PROJECT_UTILS_H


#include <openssl/ec.h>
#include <vector>
#include <functional>
#include <memory>


namespace openssl {


    /** custom BIO deleter for correct free resources **/
    typedef std::function<void(BIO*)> DeleterBIO;
    /** smart pointer for BIO with custom deleter **/
    typedef std::unique_ptr<BIO,DeleterBIO> BIOUniquePtr;


    /** custom EC_KEY deleter for correct free resources **/
    typedef std::function<void(EC_KEY*)> DeleterEC_KEY;
    /** smart pointer for BIO with custom deleter **/
    typedef std::unique_ptr<EC_KEY,DeleterEC_KEY> EC_KEYUniquePtr;


    /** custom EC_GROUP deleter for correct free resources **/
    typedef std::function<void(EC_GROUP*)> DeleterEC_GROUP;
    /** smart pointer for EC_GROUP with custom deleter **/
    typedef std::unique_ptr<EC_GROUP,DeleterEC_GROUP> EC_GROUPUniquePtr;


    /** custom EVP_MD_CTX deleter for correct free resources **/
    typedef std::function<void(EVP_MD_CTX*)> DeleterEVP_MD_CTX;
    /** smart pointer for EVP_MD_CTX with custom deleter **/
    typedef std::unique_ptr<EVP_MD_CTX,DeleterEVP_MD_CTX> EVP_MD_CTXUniquePtr;


    /** typedef for vectors **/
    typedef std::vector<char > vectorChar;
    typedef std::vector<unsigned char> vectorUchar;

    /** struct hold Key(private|public) as PEM and DER formats **/
    struct Key {
        BIOUniquePtr bioPem;
        BIOUniquePtr bioDer;
        vectorChar vectorPem;
        vectorUchar vectorDer;
    };


    ///help functions which returns correspond smartPointer
    BIOUniquePtr createUniqueBIO();
    EC_KEYUniquePtr createUniqueEC_KEY();
    EC_GROUPUniquePtr createUniqueEC_GROUP(int curveId);
    EVP_MD_CTXUniquePtr createUniqueEVP_MD_CTX();


    ///help functions which returns correspond custom deleter
    DeleterBIO deleterBIO();
    DeleterEC_KEY deleterEC_KEY();
    DeleterEC_GROUP deleterEC_GROUP();
    DeleterEVP_MD_CTX deleterEVP_MD_CTX();



}


#endif //PROJECT_UTILS_H
