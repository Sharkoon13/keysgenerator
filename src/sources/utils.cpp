//
// Created by iddqd13 on 5/21/18.
//

#include <utils.h>
#include <QtCore/QDebug>
#include <openssl/evp.h>

namespace openssl {



    BIOUniquePtr createUniqueBIO() {
        auto deleter = deleterBIO();
        BIOUniquePtr ptr( BIO_new( BIO_s_mem() ) , deleter );
        return std::move(ptr);
    }

    EC_KEYUniquePtr createUniqueEC_KEY() {
        auto deleter = deleterEC_KEY();
        EC_KEYUniquePtr ptr( EC_KEY_new() , deleter );
        return std::move(ptr);
    }

    EC_GROUPUniquePtr createUniqueEC_GROUP(int curveId) {
        //get custom deleter
        auto deleter = deleterEC_GROUP();

        //recreate smart pointer with custom deleter
        EC_GROUPUniquePtr ptr( EC_GROUP_new_by_curve_name(curveId) , deleter );

        return std::move(ptr);
    }

    EVP_MD_CTXUniquePtr createUniqueEVP_MD_CTX() {
        auto deleter = deleterEVP_MD_CTX();
        EVP_MD_CTXUniquePtr ptr;
#if OPENSSL_VERSION_NUMBER < 0x10100000L
        ptr = EVP_MD_CTXUniquePtr(EVP_MD_CTX_create() , deleter) ;
#else
        ptr = EVP_MD_CTXUniquePtr( EVP_MD_CTX_new() , deleter ) ;
#endif
        return std::move(ptr);
    }




    DeleterBIO deleterBIO()
    {
        return [](BIO* bio){
            if ( bio ) {
                BIO_free(bio);
                qDebug() << "free BIO object...done";
            }
        };
    }

    DeleterEC_KEY deleterEC_KEY()
    {
        return [](EC_KEY* ecKey){
            if ( ecKey ) {
                EC_KEY_free(ecKey);
                qDebug() << "free EC_KEY object...done";
            }
        };
    }

    DeleterEC_GROUP deleterEC_GROUP()
    {
        return [](EC_GROUP* ecGroup){
            if ( ecGroup ) {
                EC_GROUP_free(ecGroup);
                qDebug() << "free EC_GROUP object...done";
            }
        };
    }


    DeleterEVP_MD_CTX deleterEVP_MD_CTX() {
        return [](EVP_MD_CTX* mdCtx){
            if ( mdCtx ) {

#if OPENSSL_VERSION_NUMBER < 0x10100000L
                EVP_MD_CTX_destroy(mdCtx);
#else
                EVP_MD_CTX_free(mdCtx);
#endif
                qDebug() << "free EVP_MD_CTX object...done";
            }
        };
    }

}