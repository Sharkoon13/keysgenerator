//
// Created by iddqd13 on 5/17/18.
//

#include <KeysGenerator.h>
#include <QtCore/QTextStream>
#include <QtCore/QDebug>

#include <openssl/bio.h>
#include <openssl/err.h>
#include <openssl/ec.h>
#include <openssl/pem.h>
#include <openssl/evp.h>
#include <openssl/ripemd.h>
#include <QtCore/QFile>
#include <QtCore/QDir>

/** count of bytes taken from public key for generating address. first byte = 0x04, 32 - x, 32 - y **/
constexpr int LASTPARTBYTES = 65;

namespace openssl {


    KeysGenerator::KeysGenerator(const QString &outDir) : m_outDir(outDir) {
        init();
    }


    void KeysGenerator::init() {

        if ( !m_outDir.isEmpty() )
        {  //checking path for absolute
            QDir dir( m_outDir );
            if ( dir.isRelative() ) {
                m_outDir = dir.absolutePath();
                if ( !dir.exists() ) {
                    if ( !dir.mkpath(m_outDir) ) {
                        qWarning() << "Error while makind directory " << m_outDir;
                        m_outDir.clear();
                    }
                }
                std::string outdir = m_outDir.toStdString();
                m_outDir.size();
            }
        }

        //get id for prime256v1 curve group
        auto curveId = NID_X9_62_prime256v1;

        //recreate smart pointer with custom deleter
        m_curveGroup = createUniqueEC_GROUP(curveId);

        if (!m_curveGroup) {
            qWarning() << "unable to create curve NID_X9_62_prime256v1";
            return;
        }

        //allocate BIO objects for private key (on memory)
        m_private.bioDer = createUniqueBIO();
        m_private.bioPem = createUniqueBIO();

        //allocate BIO objects for public key (on memory)
        m_public.bioDer = createUniqueBIO();
        m_public.bioPem = createUniqueBIO();

        int asn1_flag = OPENSSL_EC_NAMED_CURVE;
        m_form = POINT_CONVERSION_UNCOMPRESSED;

        /*set flag ???*/
        EC_GROUP_set_asn1_flag(m_curveGroup.get(), asn1_flag);
        /*set point conversion form ?*/
        EC_GROUP_set_point_conversion_form(m_curveGroup.get(), m_form);

        /*no seed*/
        EC_GROUP_set_seed(m_curveGroup.get(), NULL, 0);

        //allocate EC_Key object
        m_eckey = createUniqueEC_KEY();

        ///checking object
        if (!m_eckey) {
            qWarning() << "eckey is NULL";
            return;
        }

        if (EC_KEY_set_group(m_eckey.get(), m_curveGroup.get()) == 0) {
            qWarning() << "unable to set group when generating key";
            return;
        }
    }

    void KeysGenerator::generateKeys() {

        if ( !m_eckey || !m_curveGroup ) {
            qWarning() << "EC_KEY object or EC_GROUP object is empty!";
            return;
        }

        ///main method for generate private key
        if (!EC_KEY_generate_key(m_eckey.get())) {
            qWarning() << "unable to generate key!";
            return;
        }

        ///read private key as DER format
        int res = i2d_ECPrivateKey_bio(m_private.bioDer.get(), m_eckey.get() );

        qDebug() << "Get result for PEM_write_bio_ECPrivateKey : %d" << res ;

        ///read private key as PEM format
        res = PEM_write_bio_ECPrivateKey(m_private.bioPem.get(), m_eckey.get(), NULL,
                                         NULL, 0, NULL, NULL);

        qDebug() << "Get result for PEM_write_bio_ECPrivateKey : %d" << res ;

        if (EC_KEY_check_key(m_eckey.get()) == 1) {
            qDebug() << "EC Key valid";
        } else {
            qDebug() << "EC Key Invalid";
        }

        ///generate public key as DER format
        res = i2d_EC_PUBKEY_bio( m_public.bioDer.get() , m_eckey.get() );

        qDebug() << "Get result for i2d_EC_PUBKEY_bio : %d" << res ;

        ///generate public key as PEM format
        res = PEM_write_bio_EC_PUBKEY( m_public.bioPem.get() , m_eckey.get() );

        qDebug() << "Get result for PEM_write_bio_EC_PUBKEY : %d" <<  res ;

        readBioToVector( m_private.bioPem , m_private.vectorPem );
        printValue( m_private.vectorPem , QStringLiteral( "Private Key as PEM" ) );
        readBioToVector( m_private.bioDer , m_private.vectorDer );
        printValue( m_private.vectorDer , QStringLiteral( "Private Key as DER" ) );

        readBioToVector( m_public.bioPem , m_public.vectorPem );
        printValue( m_public.vectorPem , QStringLiteral( "Public Key as PEM" ) );
        readBioToVector( m_public.bioDer , m_public.vectorDer);
        printValue( m_public.vectorDer , QStringLiteral( "Public Key as DER" ) );


        vectorUchar publicKeyPart(LASTPARTBYTES);

        ///check size is greater than LASTPARTBYTES ( 65 bytes )
        if ( m_public.vectorDer.size() > LASTPARTBYTES )
        { //get last 65 bytes  from public key.
            auto lastPos = m_public.vectorDer.size() - 1;
            auto startPos = m_public.vectorDer.size() - 1 - LASTPARTBYTES;
            unsigned long lidx = 0;
            for ( auto idx = startPos ; idx < m_public.vectorDer.size() ; ++idx , ++lidx ) {
                publicKeyPart[lidx] = m_public.vectorDer[idx];
            }
        }

        printValue( publicKeyPart , QStringLiteral( "Public Key[last 65 bytes]" ) );
        vectorUchar publicKeyPartSHA256;

        calculateSHA256(publicKeyPart , publicKeyPartSHA256 );

        printValue( publicKeyPartSHA256 , QStringLiteral( "publicKeyPartSHA256"  ) );

        vectorUchar publicKeyPartRmd160;

        calculateRmd160( publicKeyPartSHA256 , publicKeyPartRmd160 );

        printValue( publicKeyPartRmd160 , QStringLiteral( "publicKeyPartRmd160"  ) );

        vectorUchar publicKeyPartRmd160WithNull(1);
        publicKeyPartRmd160WithNull[0] = 0;
        publicKeyPartRmd160WithNull.insert(publicKeyPartRmd160WithNull.end() , publicKeyPartRmd160.begin() , publicKeyPartRmd160.end() );

        printValue( publicKeyPartRmd160WithNull, QStringLiteral( "publicKeyPartRmd160WithNull" ) );

        vectorUchar publicKeyPartRmd160SHA256;

        calculateSHA256( publicKeyPartRmd160WithNull , publicKeyPartRmd160SHA256 );

        printValue( publicKeyPartRmd160SHA256, QStringLiteral( "publicKeyPartRmd160SHA256" )  );

        vectorUchar publicKeyPartRmd160SHA256SHA256;

        calculateSHA256( publicKeyPartRmd160SHA256 , publicKeyPartRmd160SHA256SHA256 );

        printValue( publicKeyPartRmd160SHA256SHA256 , QStringLiteral( "publicKeyPartRmd160SHA256SHA256" )  );

        m_address.resize(1);
        m_address[0] = 0;
        m_address.insert(m_address.end() , publicKeyPartRmd160.begin() , publicKeyPartRmd160.end() );
        m_address.insert(m_address.end() , publicKeyPartRmd160SHA256SHA256.begin() , publicKeyPartRmd160SHA256SHA256.begin() + 4 );

        printValue(m_address , QStringLiteral( "publicKeyPartRmd160Result" )  );


        if ( !m_outDir.isEmpty() ) {
            saveToFiles();
        }

        printToConsole();
    }

    void KeysGenerator::calculateSHA256(vectorUchar &src, vectorUchar &dst) {

        const EVP_MD *md;
        unsigned char md_value[EVP_MAX_MD_SIZE];
        md = EVP_sha256();
        uint md_len;
        if(!md) {
            qWarning() << "Error on EVP_sha256";
            return;
        }

        auto mdctx = createUniqueEVP_MD_CTX();

        EVP_DigestInit_ex(mdctx.get(), md, NULL);
        EVP_DigestUpdate(mdctx.get(), src.data(), src.size());
        EVP_DigestFinal_ex(mdctx.get(), md_value, &md_len);

        dst.resize( md_len );
        for ( uint idx = 0 ; idx < md_len ; ++idx ) {
            dst[idx] = md_value[idx];
        }
    }

    void KeysGenerator::calculateRmd160(vectorUchar &src, vectorUchar &dst) {

        RIPEMD160_CTX c{};
        unsigned char md[RIPEMD160_DIGEST_LENGTH];
        RIPEMD160_Init(&c);
        RIPEMD160_Update(&c,src.data(),src.size());
        RIPEMD160_Final(&(md[0]),&c);

        dst.resize(RIPEMD160_DIGEST_LENGTH);

        for (uint i = 0 ; i < RIPEMD160_DIGEST_LENGTH; ++i) {
            dst[i] = md[i];
        }

    }

    void KeysGenerator::readBioToVector(BIOUniquePtr &input, vectorUchar &output) {

        vectorUchar buffer;
        auto countOfBytes = BIO_pending(input.get());
        buffer.resize( countOfBytes + 1);
        std::fill(buffer.begin(),buffer.end() , 0 );
        BIO_read(input.get(), buffer.data(), countOfBytes);
        //zero terminated string
        buffer[countOfBytes] = '\0';

        output.insert(output.begin() , buffer.begin() , buffer.end());
    }

    void KeysGenerator::readBioToVector(BIOUniquePtr &input, vectorChar &output) {
        vectorChar buffer;
        auto countOfBytes = BIO_pending(input.get());
        buffer.resize( countOfBytes + 1);  //for null-terminate symbol
        std::fill(buffer.begin(),buffer.end() , 0 );
        BIO_read(input.get(), buffer.data(), countOfBytes);
        //zero terminated string
        buffer[countOfBytes] = '\0';

        output.insert(output.begin() , buffer.begin() , buffer.end());
    }


    QString
    KeysGenerator::convertToString(vectorUchar &data, const QString &optionalName, const QString &optionalPrefix) {

        QString message;
        if ( !optionalName.isEmpty() ) {
            message.append(optionalName);
            message.append('\n');
        }
        if (! optionalPrefix.isEmpty() ) {
            message.append(optionalPrefix);
        }
        for ( uint idx = 0 ; idx < data.size() ; ++idx ) {
            if ( data[idx] < 16 ) {
                message.append('0');
            }
            message.append( QString::number( data [idx] , 16 ) );
        }
        message.append('\n');
        return message;
    }

    QString
    KeysGenerator::convertToString(vectorChar &data, const QString &optionalName, const QString &optionalPrefix) {
        QString message;
        if ( !optionalName.isEmpty() ) {
            message.append(optionalName);
            message.append('\n');
        }
        for ( uint idx = 0 ; idx < data.size() ; ++idx ) {
            message.append( data [idx] );
        }

        message.append('\n');
        return message;
    }


    void KeysGenerator::printValue(vectorChar &data, const QString &optionalName, const QString &optionalPrefix) {

        auto message = convertToString( data , optionalName , optionalPrefix );
        qDebug() << message;
    }

    void KeysGenerator::printValue(vectorUchar &data, const QString &optionalName, const QString &optionalPrefix) {
        auto message = convertToString( data , optionalName , optionalPrefix );
        qDebug() << message;
    }


    void KeysGenerator::printToConsole() {
        printValue( m_private.vectorPem , QStringLiteral("Private Key(PEM): "));
        printValue( m_private.vectorDer , QStringLiteral("Private Key(DER): "));

        printValue( m_public.vectorPem , QStringLiteral("Private Key(PEM): "));
        printValue( m_public.vectorDer , QStringLiteral("Private Key(DER): "));

        printValue( m_address , QStringLiteral("Address: ") , QStringLiteral("0x"));
    }

    void KeysGenerator::saveToFiles() {

        QString addressKeyPath = m_outDir + QStringLiteral("/address.txt");
        QString privateKeyPathPem = m_outDir + QStringLiteral("/private.pem");
        QString privateKeyPathDer = m_outDir + QStringLiteral("/private.der");
        QString publicKeyPathPem = m_outDir + QStringLiteral("/public.pem");
        QString publicKeyPathDer = m_outDir + QStringLiteral("/public.der");

        ///save to file this.
        ///also check others...
        {  //save address
            auto addressString = convertToString( m_address );
            addressString.prepend("0x");
            if (! saveFile( addressKeyPath , addressString ) ) {
                qWarning("Error while writing output address file!");
            }
        }
        {  //save address
            auto keyString = convertToString( m_private.vectorPem );
            if (! saveFile( privateKeyPathPem , keyString ) ) {
                qWarning("Error while writing output privateKeyPathPem file!");
            }
        }
        {  //save address
            auto keyString = convertToString( m_private.vectorDer );
            ///fix last zero symbol for output
            int pos = keyString.lastIndexOf(QStringLiteral("00\n"));
            if ( pos != -1  ) {
                keyString = keyString.left(pos);
                keyString.append('\n');
            }
            if (! saveFile( privateKeyPathDer , keyString ) ) {
                qWarning("Error while writing output privateKeyPathDer file!");
            }
        }
        {  //save address
            auto keyString = convertToString( m_public.vectorPem );
            if (! saveFile( publicKeyPathPem , keyString ) ) {
                qWarning("Error while writing output publicKeyPathPem file!");
            }
        }
        {  //save address
            auto keyString = convertToString( m_public.vectorDer );
            ///fix last zero symbol for output
            int pos = keyString.lastIndexOf(QStringLiteral("00\n"));
            if ( pos != -1  ) {
                keyString = keyString.left(pos);
                keyString.append('\n');
            }
            if (! saveFile( publicKeyPathDer , keyString ) ) {
                qWarning("Error while writing output publicKeyPathDer file!");
            }
        }
    }


    bool KeysGenerator::saveFile(const QString &fileName, const QString &content) {
        QFile file(fileName);
        if ( file.open(QIODevice::WriteOnly) ) {
            auto stdString = content.toStdString();
            file.write( stdString.c_str() );
            qDebug() << " write to file " << fileName;
            return true;
        }
        return false;
    }

}
