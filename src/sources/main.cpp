#include <iostream>

#include <QtCore/QCoreApplication>
#include <QtCore/QCommandLineParser>
#include <QtCore/QFile>

#include <QtCore/QFileInfo>

#include <KeysGenerator.h>



//enum CommandLineParseResult
//{
//    CommandLineOk,
//    CommandLineError,
//    CommandLineVersionRequested,
//    CommandLineHelpRequested
//};
//
///** parse input arguments  **/
//CommandLineParseResult parseCommandLine(QCommandLineParser &parser, InputInfo *info , QString *errorMessage)
//{
//    parser.setSingleDashWordOptionMode(QCommandLineParser::ParseAsLongOptions);
//    const QCommandLineOption fileOption( QStringLiteral(u"i") , QStringLiteral(u"text file contains urls for creating sitemap."), QStringLiteral(u"file")) ;
//    parser.addOption(fileOption);
//    const QCommandLineOption formatOption( QStringLiteral(u"f") , QStringLiteral(u"output file format(xml|txt);") , QStringLiteral(u"format"));
//    parser.addOption(formatOption);
//    const QCommandLineOption outputOption( QStringLiteral(u"o") , QStringLiteral(u"output sitemap file."), QStringLiteral(u"out")) ;
//    parser.addOption(outputOption);
//    const QCommandLineOption depthOption( QStringLiteral(u"d") , QStringLiteral(u"search depth ( default value = 2 , -1 is unlimited)."), QStringLiteral(u"depth")) ;
//    parser.addOption(depthOption);
//    const QCommandLineOption errorOption( QStringLiteral(u"e") , QStringLiteral(u"output errors file (default errors.log)"), QStringLiteral(u"err")) ;
//    parser.addOption(errorOption);
//    const QCommandLineOption contentOption( QStringLiteral(u"c") , QStringLiteral(u"content-filters fileName (default content-filters.txt"), QStringLiteral(u"content-filter")) ;
//    parser.addOption(contentOption);
//    const QCommandLineOption limitOption( QStringLiteral(u"l") , QStringLiteral(u"limit for output links count(-1 : unlimited(default), > 0 - limit"), QStringLiteral(u"limit")) ;
//    parser.addOption(limitOption);
//
//    const QCommandLineOption helpOption = parser.addHelpOption();
//    const QCommandLineOption versionOption = parser.addVersionOption();
//
//    if (!parser.parse(QCoreApplication::arguments())) {
//        *errorMessage = parser.errorText();
//        return CommandLineError;
//    }
//
//    if (parser.isSet(versionOption))
//        return CommandLineVersionRequested;
//
//    if (parser.isSet(helpOption))
//        return CommandLineHelpRequested;
//
//    if ( parser.isSet(outputOption) ) {
//        info->fileNameOut = parser.value( outputOption );
//    }
//    if ( parser.isSet(errorOption) ) {
//        info->fileNameErr = parser.value( errorOption );
//    }
//    if ( parser.isSet(contentOption) ) {
//        info->fileNameContentFilter = parser.value( contentOption );
//    }
//
//    if ( parser.isSet(depthOption) ) {
//        bool ok = false;
//        info->searchDepth = parser.value( depthOption ).toInt(&ok);
//        if ( !ok ) {
//            *errorMessage = "Bad input search Depth: " + parser.value( depthOption );
//            return CommandLineError;
//        }
//    } else {
//        info->searchDepth = DEFAULT_DEPTH;
//    }
//
//    if ( parser.isSet(limitOption) ) {
//        bool ok = false;
//        info->limit = parser.value( limitOption ).toInt(&ok);
//        if ( !ok ) {
//            *errorMessage = "Bad input limit value: " + parser.value( depthOption );
//            return CommandLineError;
//        }
//    } else {
//        info->limit = DEFAULT_LIMIT;
//    }
//    if (parser.isSet(fileOption) && parser.isSet(formatOption) ) {
//        const QString file = parser.value(fileOption);
//
//        if ( !QFile::exists( file ) ) {
//            *errorMessage = "Bad input file: " + file;
//            return CommandLineError;
//        }
//        const QString format = parser.value( formatOption );
//        if ( format != QStringLiteral(u"txt") && format != QStringLiteral(u"xml") ) {
//            *errorMessage = "Bad output format: " + format;
//            return CommandLineError;
//        }
//
//        info->format = format;
//        info->file = file;
//
//        return CommandLineOk;
//    }
//
//    return CommandLineHelpRequested;
//}

int main(int argc , char* argv[] ) {

    ///construct QApplication ( without GUI )
    QCoreApplication qApplication( argc , argv );
    ///set up application name and version.
    QCoreApplication::setApplicationName("keysGenerator");
    QCoreApplication::setApplicationVersion("0.1");

//    ///initialize command line parser for getting input arguments.
//    QCommandLineParser parser;
//    InputInfo info;
//    QString errorMessage;
//
//    switch (parseCommandLine(parser, &info, &errorMessage)) {
//        case CommandLineOk:
//            break;
//        case CommandLineError:
//            fputs(qPrintable(errorMessage), stderr);
//            fputs("\n\n", stderr);
//            fputs(qPrintable(parser.helpText()), stderr);
//            return 1;
//        case CommandLineVersionRequested:
//            printf("%s %s\n", qPrintable(QApplication::applicationName()),
//                   qPrintable(QApplication::applicationVersion()));
//            return 0;
//        case CommandLineHelpRequested:
//            parser.showHelp();
//            Q_UNREACHABLE();
//    }
//
//    /** create object locally for auto destruct it. **/
//    AccessChecker counterChecker( QStringLiteral( u"DomainParser_semaphoreKey" ) ,
//                                  QStringLiteral( u"DomainParser_sharedMemKey" ) );
//    /** application object must be destroyed early RunChecker object
//     *  we can set pointer to this object **/
//    Application app(&info , &counterChecker );
//    QObject::connect( &app , &Application::sl_quit , &qApplication , &QApplication::quit );
//
//    /** checking SSL for QWebEnginePage **/
//    qDebug() << "SSL version use for build "
//             << QSslSocket::sslLibraryBuildVersionString();
//    qDebug() <<"SSL version use for run-time "
//             << QSslSocket::sslLibraryVersionNumber();

    //app.process();

    QString outDir;
    if ( argc == 2 ) {
        outDir = QString::fromLocal8Bit( argv[1] );
        std::string outdir = outDir.toStdString();
        outDir.size();
    }

    openssl::KeysGenerator generator( outDir );
    generator.generateKeys();

    return 0;
}