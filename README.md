####Description
Generator private|public|address to console or files
(if argument specified);

Example:
```
KeysGenerator 
```

will generate and show to console;

and
```
KeysGenerator keys 
```

create subdirectory keys and save data to files:
( public.pem , public.der , private.pem , private.der , address.txt )



####Building

CMake Arguments to build:

-DQT_ROOT = path to directory contains bin/qmake file
-DOPENSSL_ROOT_DIR= path to directory with OpenSSL

for example:

-DQT_ROOT=/home/building/qt/online/5.9.5/gcc_64
-DOPENSSL_ROOT_DIR=/home/building/OpenSSL/1.0.2k

######Attention: generate for msvc 2017

Example for MSVC 2017 ( launch developer console and then launch: )

add to PATH variable path to CMAKE

then, create directory on same level with directory KeysGenerator
for example "build".
then change directory to build and print:
(in example, directory D:\Projects\metahash contains subdirs: build and KeysGenerator)
Script below generate x64 project.
``` 
D:\Projects\metahash\build-msvc2017>cmake.exe -DCMAKE_GENERATOR_PLATFORM=x64 -DQT_ROOT=C:/Qt/online/5.9.5/msvc2017_64 -DOPENSSL_ROOT_DIR=D:/tmp/openssl/bin ../KeysGenerator
```